import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'aut-check', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'registrasi', loadChildren: './registrasi/registrasi.module#RegistrasiPageModule' },
  { path: 'search', loadChildren: './search/search.module#SearchPageModule' },
  { path: 'posting', loadChildren: './posting/posting.module#PostingPageModule' },
  { path: 'aut-check', loadChildren: './aut-check/aut-check.module#AutCheckPageModule' },
  { path: 'new-cars', loadChildren: './new-cars/new-cars.module#NewCarsPageModule' },
  { path: 'used-cars', loadChildren: './used-cars/used-cars.module#UsedCarsPageModule' },
  { path: 'news-detail/:ids', loadChildren: './news-detail/news-detail.module#NewsDetailPageModule' },



  { path : "**", redirectTo:'home', pathMatch: 'full'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
