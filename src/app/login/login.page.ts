import { Component, OnInit } from '@angular/core';
import { LoadingController, AlertController, Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment'
import { ApiService } from "../service/api.service"


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loading :any;
  env : any = environment
  timeClose : number = 0;

  constructor(
    public loadingController: LoadingController,
    public alertController: AlertController,
    private router : Router,
    public api : ApiService,
    private platform : Platform,
    public toastController: ToastController
  ) {
    this.platform.backButton.subscribe(()=>{
      let nav = router.url
      if(nav === "/login"){
        this.timeClose ++;
        setTimeout(()=>{this.timeClose = 0}, 2000)
        if (this.timeClose == 2) {
          navigator['app'].exitApp()
        } else {
          this.presentToast()
        }
      }
    })
   }

  ngOnInit() {
  }

  login(form: any){
    if (form.value.username === "admin" && form.value.password === "secret") {
      localStorage.setItem('auth', JSON.stringify({"data" : "data"}))
      this.router.navigate(['home/tab1'],{ replaceUrl: true })
    } else {
      this.presentAlert()
    }

    // this.presentLoading()
    // this.api.ApiLogin(form.value)
    // .then(data => {
    //     localStorage.setItem('auth', JSON.stringify(data.data))
    //     setTimeout(()=>{this.loading.dismiss()}, 500)
    //     this.router.navigate(['home/tab1'],{ replaceUrl: true })
    //   })
    //   .catch(error => {
    //     console.log(error); // error message as string
    //     setTimeout(()=>{this.presentAlert()}, 500)
    //   });
  }

async presentToast() {
    const toast = await this.toastController.create({
      message: 'Klik back lagi untuk menutup aplikasi',
      duration: 2000
    });
    toast.present();
  }

async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Mohon Tunggu',
    });
    return this.loading.present()
  }

async presentAlert() {
  // this.loading.dismiss()
      const alert = await this.alertController.create({
        header: 'Error!',
        message: 'Username & Password <strong style="color:red">Not Valid</strong>!!!',
        buttons: [{
            text: 'Ok',
            handler: () => {
                null
            }
          }
        ]
    });

      await alert.present();
    }

}
