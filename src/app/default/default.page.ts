import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-default',
  templateUrl: './default.page.html',
  styleUrls: ['./default.page.scss'],
})
export class DefaultPage implements OnInit {

  search: string = ""
  token: string = ""
  user: string = ""
  constructor(
    public router : Router
  ) { }

  ngOnInit() {
    this.getDataUser()
  }

  getDataUser(){
    let parser  = JSON.parse(localStorage.getItem('auth'))
    let auth    = JSON.parse(parser).result
    this.user = auth.user
    this.token = auth.token

  }

  Logout(){
    localStorage.clear()
    this.router.navigate(['/login'],{ replaceUrl: true })
  }



}
