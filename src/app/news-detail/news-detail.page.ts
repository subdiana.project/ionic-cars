import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.page.html',
  styleUrls: ['./news-detail.page.scss'],
})
export class NewsDetailPage implements OnInit {

  id  : any;
  item : any;

  constructor(
    private route : ActivatedRoute,
    private api : ApiService
  ) { }

  ngOnInit() {
    this.logId()
  }

  logId(){
      this.route.params.subscribe(params => this.id = params.ids)
      this.api.ApiDumyDetail(this.id)
      .then(_data =>{
        this.item = JSON.parse(_data.data)
      })
      .catch(error => console.log(error))
  }

}
