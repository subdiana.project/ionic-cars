import { Component, OnInit } from '@angular/core';


import { ApiService } from '../service/api.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-news',
  templateUrl: './news.page.html',
  styleUrls: ['./news.page.scss'],
})
export class NewsPage implements OnInit {

  news: any = [];
  load : boolean = true

  search: string = ""
  constructor(
    public api : ApiService,
    public router : Router
  ) { }

  ngOnInit() {
    this.getDataNews()
  }


  filterList(event: any){
    this.load = true
    if (event.detail.value !== "") {
      this.news = this.src(event.detail.value)
      this.load = false
    } else {
      this.getDataNews()
    }
  }

  src(val:string){
      return this.news.filter((item: { title: { toLowerCase: () => { indexOf: (arg0: string) => number; }; }; })=> {
        return item.title.toLowerCase().indexOf(val.toLowerCase()) > -1;
      });
  }
  getDataNews(){
    this.api.ApiNewDumy()
    .then(data =>
      {
        this.news = JSON.parse(data.data)
        this.load = false
      }
    )
    .catch(err => {console.log(err)})
  }

  navClick(i:string){
    if (i) {
      this.router.navigate([`news-detail/${i}`])
    }
    return false
  }

}
