import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-aut-check',
  templateUrl: './aut-check.page.html',
  styleUrls: ['./aut-check.page.scss'],
})
export class AutCheckPage implements OnInit {

  auth : any
  constructor(
    public router : Router
  ) { }

  ngOnInit() {
    this.auth = localStorage.getItem('auth')
    if (this.auth == null) {
      setTimeout(()=>this.router.navigate(["/login"], { replaceUrl: true }),1000 )
    }else{
      setTimeout(()=>this.router.navigate(["/home/tab1"], { replaceUrl: true }),1000 )
    }
  }

}
