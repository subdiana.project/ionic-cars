import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutCheckPage } from './aut-check.page';

describe('AutCheckPage', () => {
  let component: AutCheckPage;
  let fixture: ComponentFixture<AutCheckPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutCheckPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutCheckPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
