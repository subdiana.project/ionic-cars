import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsedCarsPage } from './used-cars.page';

describe('UsedCarsPage', () => {
  let component: UsedCarsPage;
  let fixture: ComponentFixture<UsedCarsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsedCarsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsedCarsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
