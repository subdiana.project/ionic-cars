import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiturePage } from './fiture.page';

describe('FiturePage', () => {
  let component: FiturePage;
  let fixture: ComponentFixture<FiturePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiturePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiturePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
