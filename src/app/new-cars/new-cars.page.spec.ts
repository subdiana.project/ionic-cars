import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCarsPage } from './new-cars.page';

describe('NewCarsPage', () => {
  let component: NewCarsPage;
  let fixture: ComponentFixture<NewCarsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCarsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCarsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
