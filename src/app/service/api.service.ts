import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'

import {HTTP} from '@ionic-native/http/ngx'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  ApiUrl:string   = environment.apiBaseUrl
  ApiDumy:string  = environment.apiDumy

  constructor(private http : HTTP) { }

  ApiLogin(args :any){
    this.http.setDataSerializer('json');
    return this.http.post(this.ApiUrl + '/login', args, {'Content-Type':'application/json'})
  }

  ApiNewDumy(){
    this.http.setDataSerializer('json');
    return this.http.get(this.ApiDumy + '/posts', {}, {'Content-Type':'application/json'})
  }

  ApiDumyDetail(id:any){
    this.http.setDataSerializer('json');
    return this.http.get(this.ApiDumy + '/posts/'+id, {}, {'Content-Type':'application/json'})
  }

}
