import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Platform, AlertController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  auth : any

  constructor(
    private router : Router,
    private alertCtrl: AlertController,
    private platform: Platform,
  ) {
    this.platform.backButton.subscribe(
      () => {
        this.checkRoute(this.router.url)
      }
    )

  }
  ngOnInit(){
    this.auth = localStorage.getItem('auth')
    if (this.auth == null) {
        this.router.navigateByUrl("/login")
    }else{
      this.router.navigateByUrl("/home/tab1")
    }
  }

 checkRoute(link:any){
   let nav = link.split('/')[1]
   if (nav === 'home') {
     // close APPS
     this.presentAlert()
     // console.log(link)
   } else {
     return false
   }
 }


 async presentAlert() {
       const alert = await this.alertCtrl.create({
         // header: 'Error!',
         message: 'Yakin ingin menutup aplikasi!!!',
         buttons: [
           {
             text: 'Ok',
             handler: () => {
                 navigator['app'].exitApp()
             }
           },
           {
             text: 'Cancel',
             handler: () => {
                 null
             }
           }
         ]
     });

       await alert.present();
     }
}
